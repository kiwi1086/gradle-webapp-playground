def triggers = []

if ("$BRANCH_NAME" == 'master' || "$BRANCH_NAME" != 'release') {
    triggers << cron('H H(2-4) * * *') // daily between 2 and 4 AM
}

properties([pipelineTriggers(triggers)])

node('master') {
    try {

        stage('Checkout') {
            echo env.BRANCH_NAME
            echo "Current Workspace: <${WORKSPACE}>"
            checkout([$class: 'GitSCM', branches: [[name: '*/' + env.BRANCH_NAME]], doGenerateSubmoduleConfigurations: false, extensions: [], submoduleCfg: [], userRemoteConfigs: [[credentialsId: 'gitlab', url: 'https://gitlab.com/kiwi1086/gradle-webapp-playground.git']]])
        }

        //updateGitlabCommitStatus name: 'build', state: 'running'

        stage('Build') {
            if (env.BRANCH_NAME != 'release') {
                echo 'Build SNAPSHOT'
                sh './gradlew clean snapshot -x check -x findbugsMain -x findbugsTest'
            } else if (env.BRANCH_NAME == 'release') {
                echo 'Build RELEASE'
                sh './gradlew clean final -x check -x findbugsMain -x findbugsTest'
            }
        }

        stage('Check') {
            sh './gradlew check -x test -x findbugsMain -x findbugsTest'
        }

        stage('Test') {
            sh './gradlew test -x findbugsMain -x findbugsTest'
        }

        stage('Build and Publish Docker Image') {
            // TODO
        }

        stage('Acceptance Test') {
            // TODO Docker Image Integration Test
        }

        stage('Publish Docker Image') {
            // TODO Publish Docker Image if previous ACC Test was successful
        }

        //updateGitlabCommitStatus name: 'build', state: 'success'
    } catch (err) {
        // TODO push BUILD Failed Msg --> Slack, E-Mail, etc...
        //updateGitlabCommitStatus name: 'build', state: 'failed'
        throw err
    } finally {
        archiveArtifacts "**/*"
        junit 'build/**/TEST-*.xml'
        jacoco changeBuildStatus: true, classPattern: '**/build/classes/java/main', maximumLineCoverage: '1', minimumLineCoverage: '0'
        //dependencyCheckPublisher(pattern: 'build/**/dependency-check-report.xml')
        echo 'Pipeline end !!!'
    }
}